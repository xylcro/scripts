﻿#Requires -RunAsAdministrator
function welcome
{
    Write-Host "Welcome to XylCro's all-in-one Cyber Ops Pinboard downloader" -ForegroundColor Yellow -BackgroundColor DarkGreen
}
function options
{
    Write-Host " "
    Write-Host "Pick an option"
    Write-Host "-----------------------------------------"
    Write-Host "[1] Start download"
    Write-Host "-----------------------------------------"
    Write-Host "Possible [B]ugs"
    Write-Host "-----------------------------------------"
    Write-Host "[E]xit"
    Write-Host "-----------------------------------------"
    $option = Read-Host -Prompt "Selction"
    if($option -eq "1")
    {
        Clear-Host
        download
	    Write-Host "All done!" -ForegroundColor Green
        Write-Host " "
        Write-Host -NoNewLine 'Press any key to go back to menu...';
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
        Start-Sleep -Seconds 3
        Clear-Host
        options
    }
    elseif($option -eq "E" -or $option -eq "e")
    {
        Clear-Host
        bye
    }
    elseif($option -eq "B" -or $option -eq "b")
    {
        Clear-Host
        Write-Host "Possible bugs include:"
        Write-Host " "
        Write-Host "- Google MAY block downloads if it senses that a script is behind the downloads... There is nothing I can do about that... (All worked fine when I tested it)"
        Write-Host " "
        Write-Host "- If Google changes the way download links work, this script will break, as it relies on the way the URLs are written. If Google does change the URLs, I can change it in the script"
        Write-Host " "
        Write-Host "- Getting files from external sources like Cisco *may* not work all the time. This script uses a pretty janky way of getting the URL from a Google redirect, but on my machine it worked lol"
        Write-Host " "
        Write-Host -NoNewLine 'Press any key to go back to menu...';
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
        Clear-Host
        options
    }
    else
    {
        Write-Host "That's not a valid option" -ForegroundColor Red -BackgroundColor Yellow
        Start-Sleep -Seconds 2
        Clear-Host
        options
    }
}
function bye
{
    Write-Host "Thank you, come again!"
    Write-Host " "
    Write-Host "    by"
    Write-Host "
    ____  ___       .__  _________                
    \   \/  /___.__.|  | \_   ___ \_______  ____  
     \     /<   |  ||  | /    \  \/\_  __ \/  _ \ 
     /     \ \___  ||  |_\     \____|  | \(  <_> )
    /___/\  \/ ____||____/\______  /|__|   \____/ 
          \_/\/                  \/               "
    Start-Sleep -Seconds 3
    exit
}
Function Get-Folder($initialDirectory)
{
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms")|Out-Null

    $foldername = New-Object System.Windows.Forms.FolderBrowserDialog
    $foldername.Description = "Select the download folder"
    $foldername.rootfolder = "MyComputer"

    if($foldername.ShowDialog() -eq "OK")
    {
        $folder += $foldername.SelectedPath
    }
    else
    {
        Write-Error "Operation cancelled by user."
        exit
    }
    return $folder
}
function download
{
    Write-Host "Select download folder..."
    $outpath = Get-Folder
    Write-Host $outpath
    Start-Sleep -Seconds 1
    Write-Host "Do you want everything in their respective format (docx, pptx) [1] or everything as PDF [2]"
    do
    {
        try
        {
            [ValidatePattern('([1-2])')]$format = Read-Host -Prompt "[1-2]"
        } catch {}
    } until ($?)
    if($format -eq "1")
    {
        Write-Host "Downloading everything as their respective format!"
    }
    elseif($format -eq "2")
    {
        Write-Host "Downloading everything as PDF!"
    }
    Start-Sleep -Seconds 1
    Write-Host "Starting download..."
    Start-Sleep -Seconds 2
    Write-Host " "
    Write-Host "This can take a while! Don't worry if nothing happens." -ForegroundColor Magenta
    Start-Sleep -Seconds 2.5
    Write-Host " "
    Write-Host " "
    for ($j=0; $j -le ((Invoke-WebRequest -Uri 'https://sites.google.com/view/itfvdab1920').Links.href).Count; $j++)
    {
        Write-Progress -Activity "Scanning uasable file-links and downloading them..." -Status "$j% Complete:" -PercentComplete $j;
        $filelink = (Invoke-WebRequest -Uri 'https://sites.google.com/view/itfvdab1920').Links | Select-Object -Index $j | Select-Object -ExpandProperty href
        $filename = (Invoke-WebRequest -Uri 'https://sites.google.com/view/itfvdab1920').Links | Select-Object -Index $j | Select-Object -ExpandProperty innerText
        $invalidChars = [IO.Path]::GetInvalidFileNameChars() -join ''
        $re = "[{0}]" -f [RegEx]::Escape($invalidChars)
        $filename = $filename -replace $re
        # Write-Host "[DEBUG] $filelink" -ForegroundColor Red
        if ($filelink -like '*docs.google.com/presentation*')
        {
            $id = $filelink.Replace('https://docs.google.com/presentation/d/','').Replace('/edit?usp=sharing','').Replace('&ouid=0','')
            if($format -eq "1")
            {
                $output = "$outpath\$filename.pptx"
                $downloadlink = 'https://docs.google.com/presentation/d/'+$id+'/export/pptx?id='+$id
            }
            elseif($format -eq "2")
            {
                $output = "$outpath\$filename.pdf"
                $downloadlink = 'https://docs.google.com/presentation/d/'+$id+'/export/pdf?id='+$id
            }
            Invoke-WebRequest -Uri $downloadlink -OutFile $output
            Write-Host "$output" -ForegroundColor Yellow
        }
        elseif ($filelink -like '*docs.google.com/document*')
        {
            if($format -eq "1")
            {
                $output = "$outpath\$filename.docx"
                $downloadlink = $filelink.Replace('/d/', '/u/0/export?format=docx&id=').Replace('/edit?usp=sharing','').Replace('&ouid=0','')
            }
            elseif($format -eq "2")
            {
                $output = "$outpath\$filename.pdf"
                $downloadlink = $filelink.Replace('/d/', '/u/0/export?format=pdf&id=').Replace('/edit?usp=sharing','').Replace('&ouid=0','')
            }
            Invoke-WebRequest -Uri $downloadlink -OutFile $output
            Write-Host "$output" -ForegroundColor Yellow
        }
        elseif ($filelink -like '*drive.google.com/file*')
        {
            $output = "$outpath\$filename.pdf"
            $downloadlink = $filelink.Replace('/file/d/', '/uc?export=download&id=').Replace('/edit?usp=sharing','').Replace('/view?usp=sharing','').Replace('/view?usp=drive_open','').Replace('/edit?usp=drive_open','').Replace('&ouid=0','')
            Invoke-WebRequest -Uri $downloadlink -OutFile $output
            Write-Host "$output" -ForegroundColor Yellow
        }
        elseif ($filelink -like '*www.google.com/url*')
        {
            $output = "$outpath\$filename.pdf"
            $response = Invoke-WebRequest -Method GET -Uri $filelink -MaximumRedirection 0 -ErrorAction Ignore
            if ($response.Links.href -match '.pdf')
            {
                $externallink = $response.Links | Select-Object -Index 0 | Select-Object -ExpandProperty href
                Invoke-WebRequest -Uri $externallink -OutFile $output
                Write-Host "$output" -ForegroundColor Yellow
            }
            <#
            elseif ($response.Headers.Location -ne "")
            {
                $externallink = $response.Headers.Location
                if ($response.Links.href -match '.pdf')
                {
                    Invoke-WebRequest -Uri $externallink -OutFile $output
                    Write-Host "$output" -ForegroundColor Yellow
                }
            }
            #>
        }
        elseif ($filelink -like '*drive.google.com/open?*')
        {
            $response = Invoke-WebRequest -Method HEAD $filelink -MaximumRedirection 0 -ErrorAction Ignore
            $code = $response.StatusCode
            if($code -eq 307)
            {
                $newlocation = $response.Headers.Location
            }

            if ($newlocation -like '*docs.google.com/presentation*')
            {
                $id = $newlocation.Replace('https://docs.google.com/presentation/d/','').Replace('/edit?usp=sharing','').Replace('/view?usp=drive_open','').Replace('/edit?usp=drive_open','').Replace('&ouid=0','')
                if($format -eq "1")
                {
                    $output = "$outpath\$filename.pptx"
                    $downloadlink = 'https://docs.google.com/presentation/d/'+$id+'/export/pptx?id='+$id
                }
                elseif($format -eq "2")
                {
                    $output = "$outpath\$filename.pdf"
                    $downloadlink = 'https://docs.google.com/presentation/d/'+$id+'/export/pdf?id='+$id
                }
                Invoke-WebRequest -Uri $downloadlink -OutFile $output
                Write-Host "$output" -ForegroundColor Yellow
            }
            elseif ($newlocation -like '*docs.google.com/document*')
            {
                if($format -eq "1")
                {
                    $output = "$outpath\$filename.docx"
                    $downloadlink = $newlocation.Replace('/d/', '/u/0/export?format=docx&id=').Replace('/edit?usp=sharing','').Replace('/view?usp=drive_open','').Replace('/edit?usp=drive_open','').Replace('&ouid=0','')
                }
                elseif($format -eq "2")
                {
                    $output = "$outpath\$filename.pdf"
                    $downloadlink = $newlocation.Replace('/d/', '/u/0/export?format=pdf&id=').Replace('/edit?usp=sharing','').Replace('/view?usp=drive_open','').Replace('/edit?usp=drive_open','').Replace('&ouid=0','')
                }
                Invoke-WebRequest -Uri $downloadlink -OutFile $output
                Write-Host "$output" -ForegroundColor Yellow
            }
            elseif ($newlocation -like '*drive.google.com/file*')
            {
                $output = "$outpath\$filename.pdf"
                $downloadlink = $newlocation.Replace('/file/d/', '/uc?export=download&id=').Replace('/edit?usp=sharing','').Replace('/view?usp=drive_open','').Replace('/edit?usp=drive_open','').Replace('&ouid=0','')
                Invoke-WebRequest -Uri $downloadlink -OutFile $output
                Write-Host "$output" -ForegroundColor Yellow
            }
        }
    }
}

# Start program
welcome
options