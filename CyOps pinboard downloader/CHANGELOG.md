# Changelog CyOps Pinboard downloader

## 1.4.2pb
- Added a progress bar while downloading

## 1.4.2
- Replaced aliasses like 'clear' and 'select' to their full contents

## 1.4.1
- Removed automatically going back to menu after completion, now it's press any key to go back
- Removed file links from download screen to make it prettier. Now it's just the file path and filename

## 1.4
- Added the option to select if you want every file as their respective format (docx, pptx) or as PDF

## 1.3.2
- Added #Requires -RunAsAdministrator

## 1.3.1
- Removed NOTE at start screen

## 1.3
- Added external file scraper

## 1.2
- Bugfix

## 1.1
- Bugfix

## 1.0
- Initial release