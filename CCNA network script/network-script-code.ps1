﻿#Requires -RunAsAdministrator
function welcome
{
    Write-Host "Welcome to XylCro's all-in-one network script for CCNA" -ForegroundColor Yellow -BackgroundColor DarkGreen
}
function options
{
    Write-Host "Pick an option"
    Write-Host "-----------------------------------------"
    Write-Host "[1] Set IP settings & Disable Firewall"
    Write-Host "[2] Only set IP settings"
    Write-Host "[3] Only disable Firewall"
    Write-Host "-----------------------------------------"
    Write-Host "[T]est IP settings (ipconfig)"
    Write-Host "-----------------------------------------"
    Write-Host "[4] Remove IP settings & Enable Firewall"
    Write-Host "[5] Only remove IP settings"
    Write-Host "[6] Only enable Firewall"
    Write-Host "-----------------------------------------"
    Write-Host "Known [B]ugs"
    Write-Host "-----------------------------------------"
    Write-Host "[E]xit"
    Write-Host "-----------------------------------------"
    $option = Read-Host -Prompt "Selction"
    if($option -eq "1")
    {
        Clear-Host
        cable
        Write-Host "Please wait..."
        Start-Sleep -Seconds 4
        Clear-Host
        disFirewall
        Write-Host " "
        Write-Host "Going back to menu..."
        Start-Sleep -Seconds 3
        Clear-Host
        options
    }
    elseif($option -eq "2")
    {
        Clear-Host
        cable
        Write-Host " "
        Write-Host "Going back to menu..."
        Start-Sleep -Seconds 3
        Clear-Host
        options
    }
    elseif($option -eq "3")
    {
        Clear-Host
        disFirewall
        Write-Host " "
        Write-Host "Going back to menu..."
        Start-Sleep -Seconds 3
        Clear-Host
        options
    }
    elseif($option -eq "4")
    {
        Clear-Host
        remIP
        Write-Host "Please wait..."
        Start-Sleep -Seconds 4
        Clear-Host
        enFirewall
        Write-Host " "
        Write-Host "Going back to menu..."
        Start-Sleep -Seconds 3
        Clear-Host
        options
    }
    elseif($option -eq "5")
    {
        Clear-Host
        remIP
        Write-Host " "
        Write-Host "Going back to menu..."
        Start-Sleep -Seconds 3
        Clear-Host
        options
    }
    elseif($option -eq "6")
    {
        Clear-Host
        enFirewall
        Write-Host " "
        Write-Host "Going back to menu..."
        Start-Sleep -Seconds 3
        Clear-Host
        options
    }
    elseif($option -eq "E" -or $option -eq "e")
    {
        Clear-Host
        bye
    }
    elseif($option -eq "t" -or $option -eq "T")
    {
        Clear-Host
        ipconfig
        Write-Host " "
        Write-Host -NoNewLine 'Press any key to go back to menu...';
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
        Clear-Host
        options
    }
    elseif($option -eq "B" -or $option -eq "b")
    {
        Clear-Host
        Write-Host "Known bugs include:"
        Write-Host " "
        Write-Host "- After setting an IP address, when choosing '[T]est IP settings (ipconfig)', nothing will appear. Restarting the application fixes this. This does not affect the functionality of the application."
        Write-Host "- After setting an IP address PowerShell is supposed to show the user the settings that are set (see https://imgur.com/a/DcC8oAj), this does not happen but instead happens when choosing to remove the IP settings. This does not affect the functionality of the application"
        Write-Host -NoNewLine 'Press any key to go back to menu...';
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
        Clear-Host
        options
    }
    else
    {
        Write-Host "That's not a valid option" -ForegroundColor Red -BackgroundColor Yellow
        Start-Sleep -Seconds 2
        Clear-Host
        options
    }
}
function setIP
{
    Write-Host "-= Set IP settings =-"
    Start-Sleep -Seconds 1
    Write-Host " "
    Write-Host "Disabling Wi-Fi..."
    Disable-NetAdapter -Name "Wi-Fi" -Confirm:$false
    Write-Host "Wi-Fi disabled" -ForegroundColor Red
    Write-Host "Please wait..."
    Get-NetAdapter | Format-Table -Property Name, ifIndex -AutoSize
    do
    {
        try
        {
            [ValidatePattern('([0-9])')]$ifindex = Read-Host -Prompt "Type the ifIndex number of the interface you want to set the IP address to"
        } catch {}
    } until ($?)   
    Write-Host "Removing previous set IP addresses if they exist..."
    Write-Host "If there are errors here, you can ignore them" -ForegroundColor Yellow
    Start-Sleep -Milliseconds 750
    Remove-NetIPAddress -InterfaceIndex $ifindex -Confirm:$false
    Remove-NetRoute -InterfaceIndex $ifindex -Confirm:$false
    Write-Host "Disabling DHCP..."
    Start-Sleep -Milliseconds 500
    Set-NetIPInterface -InterfaceIndex $ifindex -Dhcp Disabled
    Write-Host "Configuration set to static" -ForegroundColor Red
    do
    {
        try
        {
            [ValidatePattern('^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$')]$ip = Read-Host -Prompt "IP adres"
        } catch {}
    } until ($?)   
    Write-Host "255.255.255.0   --> 24"
    Write-Host "255.255.255.128 --> 25"
    Write-Host "255.255.255.192 --> 26"
    Write-Host "255.255.255.224 --> 27"
    Write-Host "255.255.255.240 --> 28"
    Write-Host "255.255.255.248 --> 29"
    Write-Host "255.255.255.252 --> 30"
    do
    {
        try
        {
            [ValidatePattern('^([0-9]|[1-2][0-9]|3[0-2])$')]$prefix = Read-Host -Prompt "Subnet prefix"
        } catch {}
    } until ($?)  
    $setGateway = Read-Host "Set gateway? (yes/no)"
    if($setGateway -eq "y" -or $setGateway -eq "yes")
    {
        do
        {
            try
            {
                [ValidatePattern('^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$')]$gateway = Read-Host -Prompt "Default Gateway"
            } catch {}
        } until ($?)   
        Write-Host "Setting IP settings on chosen interface..."
        New-NetIPAddress -InterfaceIndex $ifindex -IPAddress $ip -PrefixLength $prefix -DefaultGateway $gateway
        Write-Host "IP settings should be set! Check this back in the menu" -ForegroundColor Green
    }
    elseif($setGateway -eq "n" -or $setGateway -eq "no")
    {
        Write-Host "Setting IP settings on chosen interface..."
        New-NetIPAddress -InterfaceIndex $ifindex -IPAddress $ip -PrefixLength $prefix
        Write-Host "IP settings should be set! Check this back in the menu" -ForegroundColor Green
    }
    else
    {
        Write-Host "Not a valid option. Resetting..." -ForegroundColor Red -BackgroundColor Yellow
        Start-Sleep -Seconds 1
        Clear-Host
        setIP
    }
}
function remIP
{
    Write-Host "-= Removing IP settings -="
    Start-Sleep -Seconds 1
    Write-Host " "
    Write-Host "Please wait..."
    Get-NetAdapter | Format-Table -Property Name, ifIndex -AutoSize
    do
    {
        try
        {
            [ValidatePattern('([0-9])')]$ifindex = Read-Host -Prompt "Type the ifIndex number of the interface you want to remove the IP address from"
        } catch {}
    } until ($?)
    Write-Host "Removing IP address and subnetmask..."
    Start-Sleep -Milliseconds 500
    Remove-NetIPAddress -InterfaceIndex $ifindex -Confirm:$false
    Write-Host "Removed IP address and subnetmask" -ForegroundColor Green
    Write-Host "Removing Gateway if set..."
    Start-Sleep -Milliseconds 500
    Remove-NetRoute -InterfaceIndex $ifindex -Confirm:$false
    Write-Host "If Gateway was not set, you'll get an error here; this is normal and can ignore it" -ForegroundColor Yellow
    Write-Host "Setting interface back to DHCP"
    Start-Sleep -Milliseconds 500
    Set-NetIPInterface -InterfaceIndex $ifindex -Dhcp Enabled
    Write-Host "DHCP set" -ForegroundColor Green
    Write-Host "Enabling Wi-Fi..."
    Enable-NetAdapter -Name "Wi-Fi" -Confirm:$false
    Write-Host "Wi-Fi enabled" -ForegroundColor Green
}
function disFirewall
{
    Write-Host "-= Disable Firewall -="
    Start-Sleep -Seconds 1
    Write-Host " "
    Write-Host "Disabling Firewall..."
    Start-Sleep -Milliseconds 500
    Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
    Write-Host "Firewall disabled" -ForegroundColor Red
}
function enFirewall
{
    Write-Host "-= Enable Firewall -="
    Start-Sleep -Seconds 1
    Write-Host " "
    Write-Host "Enabling Firewall..."
    Start-Sleep -Milliseconds 500
    Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled True
    Write-Host "Firewall enabled" -ForegroundColor Green
}
function cable
{
    Write-Host "The ethernet cable should be plugged in before continuing (in both the PC as in the router/switch)"
    $cable = Read-Host -Prompt "Is the cable plugged in? (y/n)"
    if($cable -eq "y" -or $cable -eq "yes")
    {
        setIP
    }
    elseif($cable -eq "n" -or $cable -eq "no"){
        Write-Host "Please plug in the cable in both the PC and switch/router" -ForegroundColor Red -BackgroundColor Yellow
        Write-Host "Waiting 5 seconds before asking again..."
        Start-Sleep -Seconds 5
        cable
    }
    else
    {
        cable
    }
}
function bye
{
    Write-Host "Thank you, come again!"
    Write-Host " "
    Write-Host "    by"
    Write-Host "
    ____  ___       .__  _________                
    \   \/  /___.__.|  | \_   ___ \_______  ____  
     \     /<   |  ||  | /    \  \/\_  __ \/  _ \ 
     /     \ \___  ||  |_\     \____|  | \(  <_> )
    /___/\  \/ ____||____/\______  /|__|   \____/ 
          \_/\/                  \/               "
    Start-Sleep -Seconds 3
    exit
}

welcome
options
