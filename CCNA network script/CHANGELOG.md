# Changelog CCNA Network script

## 1.4.3
- Replaced aliasses like 'clear' and 'select' to their full contents

## 1.4.2
- Changed exe to MIT copyright

## 1.4.1
- Code fix

## 1.4
- Added Bugs menu

## 1.3
- Bugfix

## 1.2
- Bugfix

## 1.1
- Bugfix

## 1.0
- Initial release