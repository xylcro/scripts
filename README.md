# XylCro's scripts
## CCNA Network Script
This script was created to quickly change or set IP settings and disable the Windows firewall without using the GUI. 
Download the compiled executable [here](https://bitbucket.org/xylcro/scripts/downloads/network-script1.4.3.exe). 

[Changelog](https://bitbucket.org/xylcro/scripts/src/master/CCNA%20network%20script/CHANGELOG.md)
### Images
![CCNA Network Script](img/network-script.png)
### Bugs
- After setting an IP address, when choosing '[T]est IP settings (ipconfig)', nothing will appear. Restarting the application fixes this. This does not affect the functionality of the application.
- After setting an IP address PowerShell is supposed to show the user the settings that are set (see https://imgur.com/a/DcC8oAj), this does not happen but instead happens when choosing to remove the IP settings. This does not affect the functionality of the application


## CyOps Pinboard downloader
I wanted a way to easily batch-download all the files on the CyberOps Google Pinboard site, so I created this script for it.
Download the compiled executable [here](https://bitbucket.org/xylcro/scripts/downloads/cyops-downloader1.4.2.exe)

I've added some code to display a progress bar while downloading, you can download the compiled version [here](https://bitbucket.org/xylcro/scripts/downloads/cyops-downloader1.4.2pb.exe).
However I noticed that the exact program is slower with progress bar than without it.

[Changelog](https://bitbucket.org/xylcro/scripts/src/master/CyOps%20pinboard%20downloader/CHANGELOG.md)
### Images
![CyOps downloader options](img/cyops-downloader1.png)
![CyOps downloader start](img/cyops-downloader2.png)
![CyOps downloader start download](img/cyops-downloader3.png)
![CyOps downloader with progress bar](img/cyops-downloaderpb.png)
### Bugs
None so far

## Google links downloader
There were some Google files that were not on the Pinboard but also on Canvas, I bundled them and made a script to download them all.
It's exactly the same script as the CyOps Pinboard downloader, only the download link is different; it uses the .html file that is available in the folder, it is hosted on my website.
Download the compiled .exe [here](https://bitbucket.org/xylcro/scripts/downloads/glinks-bulk-downloader1.0.exe).

[Changelog](https://bitbucket.org/xylcro/scripts/src/master/Glinks%20bulk%20downloader/CHANGELOG.md)
### Images
Same as CyOps Pinboard downloader
### Bugs
None so far

## Compiling
If you don't trust my compiled executables, you can always compile your own 😉

I converted/compiled the .ps1 source file to an .exe using [PS2EXE-GUI](https://gallery.technet.microsoft.com/scriptcenter/PS2EXE-GUI-Convert-e7cb69d5).

## Licence
This project is under the [MIT Licence](LICENCE)

